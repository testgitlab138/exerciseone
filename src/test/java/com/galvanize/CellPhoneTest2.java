package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class CellPhoneTest2 {

    CallingCard card1 = new CallingCard(10);
    CellPhone phone1 = new CellPhone(card1);
    CallingCard card2 = new CallingCard(20);
    CellPhone phone2 = new CellPhone(card2);
    CallingCard card3 = new CallingCard(50);
    CellPhone phone3 = new CellPhone(card3);
    @Test
    public void checkForCallingCard(){
        //setup
        CallingCard card1 = new CallingCard(10);
        CellPhone phone1 = new CellPhone(card1);
        CallingCard card2 = new CallingCard(20);
        CellPhone phone2 = new CellPhone(card2);
        CallingCard card3 = new CallingCard(50);
        CellPhone phone3 = new CellPhone(card3);
        //enact
        //assert
        assertEquals(10, phone1.card.getCentsPerMinute());
        assertEquals(20, phone2.card.getCentsPerMinute());
        assertEquals(50, phone3.card.getCentsPerMinute());
        //teardown
    }

    @Test
    public void checkToSeeIfCallActive(){
        //setup
        CellPhone phone = new CellPhone();
        CellPhone phone2 = new CellPhone();
        //enact
        phone2.callActive = true;
        //assert
        assertEquals(false, phone.callActive);
        assertEquals(true, phone2.callActive);
        //teardown
    }

    @Test
    public void checkToStartACall(){
        //Setup
        CellPhone phone = new CellPhone();
        CellPhone phone2 = new CellPhone();
        //Enact
        phone.startCall();
        //Assert
        assertEquals(true, phone.callActive);
        assertEquals(false, phone2.callActive);
        //teardown
    }

    @Test
    public void checkIfTalking(){
        //setup
        CellPhone phone = new CellPhone();
        CellPhone phone2 = new CellPhone();
        //enact
        phone2.startCall();
        //assert
        assertEquals(false, phone.isTalking());
        assertEquals(true, phone2.isTalking());
        //teardown
    }

    @Test
    public void checkIfMinutesTick(){
        //setup

        //enact

        phone2.card.addDollars(5);
        phone1.tick();
        phone1.tick();
        phone2.tick();
        phone2.tick();
        phone2.tick();
        //assert

        assertEquals(3, phone2.getCallMinutes());
        //teardown
    }

    @Test
    public void checkCallMinutesAndIfTickUsesMinutes(){
        //setup

        CallingCard card1 = new CallingCard(10);
        CellPhone phone1 = new CellPhone(card1);
        CallingCard card2 = new CallingCard(20);
        CellPhone phone2 = new CellPhone(card2);
        CallingCard card3 = new CallingCard(50);
        CellPhone phone3 = new CellPhone(card3);
        //enact
        phone1.card.addDollars(1);
        phone1.tick();
        phone2.card.addDollars(1);
        phone2.tick();
        phone2.tick();
        phone2.tick();
        phone3.card.addDollars(1);
        phone3.tick();
        phone3.tick();
        phone3.tick();

        //assert
        assertEquals(1, phone1.getCallMinutes());
        assertEquals(9, phone1.card.getRemainingMinutes());
        assertEquals(3, phone2.getCallMinutes());
        assertEquals(2, phone2.card.getRemainingMinutes());
        assertEquals(0, phone3.getCallMinutes());
        assertEquals(0, phone3.card.getRemainingMinutes());
        //teartdown
    }

    @Test
    public void checkToSeeIfMinutesRemaining(){
        //setup
        //enact
        phone3.card.addDollars(1);
        phone3.tick();
        phone3.tick();
        phone3.tick();

        //assert
        assertEquals(0, phone3.card.getRemainingMinutes());
        //teardown
    }

    @Test
    public void checkCurrCallNumber(){
        //setup
        //enact
        phone1.currCall = "123-4567";
        // assert
        assertEquals("123-4567", phone1.currCall);
    }

    @Test
    public void checkIfCallStarted(){
        //setup
        //enact
        phone1.call("123-4567");
        //assert
        assertEquals("123-4567", phone1.currCall);
        assertEquals(true, phone1.callActive);
        //teardown
    }

    @Test
    public void checkIfCallEnded(){
        //setup
        //enact
        phone1.call("123-4567");
        phone1.endCall();
        //assert
        assertEquals(false, phone1.callActive);
    }

    @Test
    public void checkGetCallHistory(){
        //setup
        //enact
        //assert
    }
    @Test
    public void checkIfCallHistoryIsSaved(){
        //setup
        //enact
        phone1.card.addDollars(5);
        phone1.call("123-4567");
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.endCall();

        phone2.card.addDollars(5);
        phone2.call("234-4567");
        phone2.tick();
        phone2.endCall();
        //assert
        assertEquals("123-4567 (3 minutes)", phone1.callHistory);
        assertEquals("234-4567 (1 minute)", phone2.callHistory);
    }

    @Test
    public void checkCallHistoryForMultipleCalls(){
        //setup
        //enact
        phone1.card.addDollars(5);
        phone1.call("123-4567");
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.endCall();

        phone1.card.addDollars(5);
        phone1.call("234-4567");
        phone1.tick();
        phone1.endCall();
        //assert
        assertEquals("123-4567 (3 minutes), 234-4567 (1 minute)", phone1.callHistory);
    }

    @Test
    public void checkCallHistoryWhenCuttOff(){
        //setup
        //enact
        phone1.card.addDollars(5);
        phone1.call("123-4567");
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.endCall();

        phone1.card.addDollars(1);
        phone1.call("234-4567");
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        phone1.tick();
        //assert
        assertEquals("123-4567 (3 minutes), 234-4567 (cut off at 10 minutes)", phone1.callHistory);
    }
}
