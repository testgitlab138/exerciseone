package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CallingCardTest {
    @Test
    public void checkForCentsPerMinute(){
        // Setup
        CallingCard card1 = new CallingCard(5);
        CallingCard card2 = new CallingCard(10);
        CallingCard card3 = new CallingCard(20);
        // Enact
        // Assert
        assertEquals(5, card1.getCentsPerMinute());
        assertEquals(10, card2.getCentsPerMinute());
        assertEquals(20, card3.getCentsPerMinute());
        // Teardown
    }

    @Test
    public void checkForMinutes(){
        // Setup
        CallingCard card1 = new CallingCard(5);
        CallingCard card2 = new CallingCard(10);
        CallingCard card3 = new CallingCard(20);
        // Enact
        card1.setCentsPerMinute(60);
        card2.setCentsPerMinute(80);
        card3.setCentsPerMinute(90);
        // Assert
        assertEquals(60, card1.getCentsPerMinute());
        assertEquals(80, card2.getCentsPerMinute());
        assertEquals(90, card3.getCentsPerMinute());
        // Teardown
    }

    @Test
    public void checkingMinutesAfterAddingDollars(){
        // Setup
        CallingCard card1 = new CallingCard(5);
        CallingCard card2 = new CallingCard(10);
        CallingCard card3 = new CallingCard(20);
        // Enact
        card1.addDollars(1);
        card2.addDollars(3);
        card3.addDollars(5);
        // Assert
        assertEquals(20, card1.getRemainingMinutes());
        assertEquals(30, card2.getRemainingMinutes());
        assertEquals(25, card3.getRemainingMinutes());
        // Teardown
    }

    @Test
    public void checkForRemainingMinutes(){
        // Setup
        CallingCard card1 = new CallingCard(5);
        CallingCard card2 = new CallingCard(10);
        CallingCard card3 = new CallingCard(20);
        // Enact
        card1.addDollars(1);
        int minutesTwenty = card1.getRemainingMinutes();
        card2.addDollars(1);
        int minutesTen = card2.getRemainingMinutes();
        card3.addDollars(1);
        int minutesFive = card3.getRemainingMinutes();
        // Assert
        assertEquals(20, minutesTwenty);
        assertEquals(10, minutesTen);
        assertEquals(5, minutesFive);
        // Teardown
    }

    @Test
    public void useACertainAmountOfMinutes(){
        // Setup
        CallingCard card1 = new CallingCard(5);
        CallingCard card2 = new CallingCard(10);
        CallingCard card3 = new CallingCard(20);
        // Enact
        card1.addDollars(1);
        card1.useMinutes(1);
        int minutesNineteen = card1.getRemainingMinutes();
        card2.addDollars(1);
        card2.useMinutes(2);
        int minutesEight = card2.getRemainingMinutes();
        card3.addDollars(1);
        card3.useMinutes(3);
        int minutesTwo = card3.getRemainingMinutes();
        // Assert
        assertEquals(19, minutesNineteen);
        assertEquals(8, minutesEight);
        assertEquals(2, minutesTwo);
        // Teardown
    }
}
