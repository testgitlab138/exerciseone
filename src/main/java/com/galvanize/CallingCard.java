package com.galvanize;

public class CallingCard {
    private int centsPerMinute;
    private int minutes;

    public int getCentsPerMinute(){
        return this.centsPerMinute;
    }

    public void setCentsPerMinute(int centsPerMinute){
        this.centsPerMinute = centsPerMinute;
    }

    public CallingCard(int centsPerMinute){
        this.centsPerMinute = centsPerMinute;
    }

    public void addDollars(int dollars){
        this.minutes = (dollars*100)/this.centsPerMinute;
    }

    public int getRemainingMinutes(){
        return this.minutes;
    }

    public void useMinutes(int minutes){
        this.minutes -= minutes;
    }
}
