package com.galvanize;

public class CellPhone {
    CallingCard card;
    boolean callActive;
    String currCall;
    String callHistory = "";

    int callMinutes;
    public CellPhone(){

    }
    public CellPhone(CallingCard card1){
        this.card = card1;
    }

    public void startCall(){
        this.callActive = true;
    }

    public boolean isTalking(){
        return callActive;
    }

    public void tick(){
        if(card.getRemainingMinutes() == 1){
            card.useMinutes(1);
            callMinutes++;
            endCall(true);
        }else if (card.getRemainingMinutes() > 1){
            card.useMinutes(1);
            callMinutes++;
        }
    }

    public String getHistory(){
        return this.callHistory;
    }
    public int getCallMinutes(){
        return callMinutes;
    }

    public void call(String phoneNumber){
        this.currCall = phoneNumber;
        this.callActive = true;
    }

    public void endCall(){
        endCall(false);
    }
    public void endCall(boolean cutOff){
        // End Call
        this.callActive = false;

        // Initialize empty string for optional characters
        String cutOffString = "";
        String plural = "";
        String comma = "";

        // Check whether to include the optional characters
        if(cutOff){ // Check if the call was cutoff
            cutOffString = "cut off at ";
        }
        if(callMinutes != 1){ // Check if the call minutes is one, if not, add an 's' to minutes
            plural = "s";
        }
        if(callHistory.equals("")){ // Check if This is the first call, if not, add a comma beforehand
            comma = ", ";
        }

        // Append to call history
        this.callHistory += comma + this.currCall + " (" + cutOffString + callMinutes + " minute" + plural + ")";
        this.callMinutes = 0;
    }
}
